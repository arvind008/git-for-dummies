# How to maintain projects

__`iiitmk-projects`__  
* owner of all groups

__`student grp head`__ 
* owner of the group
* owner of all projects in the group
* maintains master branch
* accepts requests only from develop branch

__`project head`__     
* maintainer of the project
* maintains develop branch
* accepts merge requests from other branches

__`developers`__       
* can create new branches
* can send merge request to develop branch

                  

                 

